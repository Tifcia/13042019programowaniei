import java.util.Set;
import java.util.TreeSet;

public class Country {
    Set<String> countries;

    public Country(){
        countries = new TreeSet<>();
    }

    public void addCountry(String country){
        countries.add(country);
    }

    public Set<String> getCountries(){
        return countries;
    }
}
